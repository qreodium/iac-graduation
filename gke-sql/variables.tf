variable "gcp_project_id" {
  description = "The project ID to host the cluster in"
}

variable "gcp_region" {
  description = "The region to host the cluster in"
}

variable "gcp_zones" {
  type        = list(string)
  description = "The zone to host the cluster in (required if is a zonal cluster)"
}

variable "gcp_db_password" {
  description = "Databse password"
}

variable "gcp_db_instance_name" {
  description = "Databse name"
}

variable "gcp_db_username" {
  description = "Databse username"
}
