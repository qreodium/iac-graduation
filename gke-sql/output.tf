output "gcp_db_addr" {
  value = module.sql-db_postgresql.private_ip_address
}
