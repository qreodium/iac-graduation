# gcp_project_id = from group env
# gcp_region     = from group env
gcp_zones        = ["northamerica-northeast2-a", "northamerica-northeast2-b", "northamerica-northeast2-c"]
gcp_db_instance_name = "yelb-db"
# gcp_db_username = from group env
# gcp_db_password = from group env
