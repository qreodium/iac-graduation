//Virtual Private Network
module "vpc" {
  source  = "terraform-google-modules/network/google"
  version = "~> 3.0"

  project_id                             = module.project_services.project_id
  network_name                           = "yelb-vpc"
  delete_default_internet_gateway_routes = false

  subnets = [
    {
      subnet_name   = "yelb-vpc-subnet"
      subnet_ip     = "10.0.0.0/17"
      subnet_region = var.gcp_region
    }
  ]

  secondary_ranges = {
    "yelb-vpc-subnet" = [
      {
        range_name    = "yelb-vpc-subnet-pods"
        ip_cidr_range = "192.168.0.0/18"
      },
      {
        range_name    = "yelb-vpc-subnet-services"
        ip_cidr_range = "192.168.64.0/18"
      },
    ]
  }
}

// Router for enable NAT
module "cloud_router"{
  source  = "terraform-google-modules/cloud-router/google"
  version = "~> 0.4"

  name    = "yelb-router"
  project = module.project_services.project_id
  region  = var.gcp_region
  network = module.vpc.network_name
  nats = [{
    name = "yelb-gateway"
  }]
}

// Database IP
resource "google_compute_global_address" "yelb_db_ip" {
  project       = module.project_services.project_id
  name          = "yelb-db-ip"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  network       = module.vpc.network_self_link
  address       = "10.1.0.0"
  prefix_length = 16
}

// Database IP to VPC connect
resource "google_service_networking_connection" "private_vpc_connection" {
  network                 = module.vpc.network_self_link
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.yelb_db_ip.name]
}

