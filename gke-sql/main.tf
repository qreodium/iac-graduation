// Enable APIs
module "project_services" {
  source  = "terraform-google-modules/project-factory/google//modules/project_services"
  version = "~> 11.0"

  project_id                  = var.gcp_project_id
  disable_services_on_destroy = false

  activate_apis = [
    "iam.googleapis.com",
    "compute.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "container.googleapis.com", //gke
    "servicenetworking.googleapis.com",
    "sqladmin.googleapis.com"
  ]
}

// Sleep for correct gke destroy
resource "time_sleep" "wait_10_seconds" {
  depends_on = [module.vpc]
  create_duration = "10s"
}

// GKE Cluster
module "gke" {
  source  = "terraform-google-modules/kubernetes-engine/google//modules/private-cluster"
  version = "18.0.0"

  depends_on = [time_sleep.wait_10_seconds]
  # Create an implicit dependency on service activation
  project_id                        = module.project_services.project_id
  name                              = "yelb-app"
  region                            = var.gcp_region
  zones                             = var.gcp_zones
  network                           = "yelb-vpc"
  subnetwork                        = "yelb-vpc-subnet"
  ip_range_pods                     = "yelb-vpc-subnet-pods"
  ip_range_services                 = "yelb-vpc-subnet-services"
  enable_private_nodes              = true
  master_ipv4_cidr_block            = "10.1.0.0/28"
  create_service_account            = false
  remove_default_node_pool          = true
  disable_legacy_metadata_endpoints = false
  issue_client_certificate          = true

  node_pools = [
    {
      name               = "yelb-app-nodes"
      machine_type       = "e2-small"
      autoscaling        = true
      initial_node_count = 1
      min_count          = 1
      max_count          = 3
      local_ssd_count    = 0
      disk_size_gb       = 10
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = false
    }
  ]

  node_pools_metadata = {
    yelb-app-nodes = {
      shutdown-script = file("${path.module}/data/shutdown-script.sh")
    }
  }

  node_pools_oauth_scopes = {
    yelb-app-nodes = [
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/devstorage.read_only",
    ]
  }

}

// Postgres
module "sql-db_postgresql" {
  source  = "GoogleCloudPlatform/sql-db/google//modules/postgresql"
  version = "8.0.0"

  depends_on       = [google_service_networking_connection.private_vpc_connection]
  project_id       = module.project_services.project_id
  name             = var.gcp_db_instance_name
  random_instance_name = true
  region           = var.gcp_region
  zone             = var.gcp_zones[0]
  database_version = "POSTGRES_13"
  tier             = "db-custom-2-8192"
  deletion_protection = false
  disk_autoresize  = true
  user_name        = var.gcp_db_username
  user_password    = var.gcp_db_password
  ip_configuration = {
    "authorized_networks" : [],
    "ipv4_enabled"    = false,
    "private_network" = module.vpc.network_self_link,
    "require_ssl" : null
  }

}
