//Enable APIs
module "project_services" {
  source  = "terraform-google-modules/project-factory/google//modules/project_services"
  version = "~> 11.0"
  project_id = var.gcp_project_id
  disable_services_on_destroy = false
  activate_apis = [
    "monitoring.googleapis.com",
    "logging.googleapis.com"
  ]
}

resource "google_monitoring_dashboard" "dashboard1" {
  dashboard_json = file("${path.module}/dashboards/gce-vm-instance-monitoring.json")
  project        = var.gcp_project_id
}

resource "google_monitoring_dashboard" "dashboard2" {
  dashboard_json = file("${path.module}/dashboards/gke-cluster-monitoring.json")
  project        = var.gcp_project_id
}

resource "google_monitoring_dashboard" "dashboard3" {
  dashboard_json = file("${path.module}/dashboards/cloudsql-usage.json")
  project        = var.gcp_project_id
}
