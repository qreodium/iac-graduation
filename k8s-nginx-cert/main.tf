# gcp_project_id = from group env
# gcp_region     = from group env
# gcp_db_addr    = generate in ci from output of gke-sql


module "gke_auth" {
  source  = "terraform-google-modules/kubernetes-engine/google//modules/auth"
  version = "~> 10.0"

  project_id   = var.gcp_project_id
  cluster_name = "yelb-app"
  location     = var.gcp_region
}

provider "helm" {
  kubernetes {
    cluster_ca_certificate = module.gke_auth.cluster_ca_certificate
    host                   = module.gke_auth.host
    token                  = module.gke_auth.token
  }
}

provider "kubernetes" {
  cluster_ca_certificate = module.gke_auth.cluster_ca_certificate
  host                   = module.gke_auth.host
  token                  = module.gke_auth.token
}

resource "kubernetes_namespace" "yelb-graduation" {
  metadata {
    name = "yelb-graduation"
  }
}

resource "kubernetes_secret" "db-addr" {
  depends_on = [kubernetes_namespace.yelb-graduation]
  metadata {
    name      = "db-addr"
    namespace = "yelb-graduation"
  }

  data = {
    addr = var.gcp_db_addr
  }

  type = "Opaque"
}


data "google_compute_address" "yelb-app-ip" {
  name = "yelb-app-ip"
}

resource "helm_release" "nginx-ingress" {
  name             = "nginx-release"
  repository       = "https://helm.nginx.com/stable"
  chart            = "nginx-ingress"
  namespace        = "nginx-ingress"
  create_namespace = true

  set {
    name  = "controller.service.loadBalancerIP"
    value = data.google_compute_address.yelb-app-ip.address
  }
}

resource "kubernetes_namespace" "cert-ns" {
  metadata {
    name = "cert-manager"
  }
}

module "cert-manager" {
  depends_on = [kubernetes_namespace.cert-ns]
  source     = "basisai/cert-manager/helm"
  version    = "0.1.3"

  chart_namespace = "cert-manager"
}

module "monitoring_dashboards" {
  source         = "./monitoring"
  gcp_project_id = var.gcp_project_id
}
