//static ip for app
resource "google_compute_address" "yelb_app_ip" {
  region       = var.gcp_region
  name         = "yelb-app-ip"
  project      = var.gcp_project_id
  description  = "Yelb Ingress IP"
}
