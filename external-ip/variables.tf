variable "gcp_project_id" {
  description = "The project ID to host the cluster in"
}

variable "gcp_region" {
  description = "The region to host the cluster in"
}
