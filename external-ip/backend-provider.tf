terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/32173684/terraform/state/ip-address-state"
    lock_address   = "https://gitlab.com/api/v4/projects/32173684/terraform/state/ip-address-state/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/32173684/terraform/state/ip-address-state/lock"
    username       = "qreodium"
    # password = from gitlab env
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = "5"
  }

  required_version = ">= 0.13.0"
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.90.1"
    }
  }
}

provider "google" {
  project     = var.gcp_project_id
  region      = var.gcp_region
  credentials = file("../serviceaccount.json")
}
