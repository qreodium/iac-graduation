module "project_services" {
  source  = "terraform-google-modules/project-factory/google//modules/project_services"
  version = "~> 11.0"

  project_id                  = var.gcp_project_id
  disable_services_on_destroy = false
  activate_apis = [
    "cloudresourcemanager.googleapis.com",
    "iam.googleapis.com",
    "compute.googleapis.com"
  ]
}


# Install the GitLab CI Runner infrastructure
module "gitlab-runner" {
  depends_on = [module.project_services]
  source  = "./runner-module"

  gcp_project             = module.project_services.project_id
  gcp_zone                = var.gcp_zone
  gcp_region              = var.gcp_region
  ci_runner_instance_type = var.ci_runner_instance_type
  ci_runner_disk_size     = var.ci_runner_disk_size
  gitlab_url              = var.gitlab_url
  ci_token                = var.ci_token
  ci_runner_gitlab_tags   = var.ci_runner_gitlab_tags
  ci_concurrency          = 4
  docker_privileged       = true
}
