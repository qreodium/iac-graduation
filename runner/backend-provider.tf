#terraform state to gitlab
terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/32173684/terraform/state/runner-state"
    lock_address   = "https://gitlab.com/api/v4/projects/32173684/terraform/state/runner-state/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/32173684/terraform/state/runner-state/lock"
    username       = "qreodium"
    # password = from gitlab env
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = "5"
  }

  required_version = ">= 0.13"
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.43, < 5.0"
    }
  }
}

# Configure GCP provider
provider "google" {
  project     = var.gcp_project_id
  region      = var.gcp_region
  credentials = file("./serviceaccount.json")
}
