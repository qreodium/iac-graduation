# gcp_project_id = from group env
# gcp_region  = from group env
gcp_zone   = "northamerica-northeast2-a"
gitlab_url = "https://gitlab.com/"
# ci_token   = from gitlab env
ci_runner_gitlab_tags   = "gcp-runner"
ci_runner_disk_size     = "20"
ci_runner_instance_type = "g1-small"
